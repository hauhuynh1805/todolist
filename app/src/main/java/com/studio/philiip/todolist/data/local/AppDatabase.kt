package com.studio.philiip.todolist.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.studio.philiip.todolist.data.local.dao.TaskDao
import com.studio.philiip.todolist.data.local.entity.TaskEntity

@Database(entities = [TaskEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun taskDao(): TaskDao
}