package com.studio.philiip.todolist.data.local.entity

data class Links(val type: String, val description: String, val link: String) {
}