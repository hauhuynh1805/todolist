package com.studio.philiip.todolist

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.studio.philiip.todolist.databinding.ActivityHomeBinding


class MainActivity : AppCompatActivity() {

    private lateinit var activityHomeBinding: ActivityHomeBinding;
    private lateinit var handlers: MyClickHandlers

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        activityHomeBinding = DataBindingUtil.setContentView(this, R.layout.activity_home)
        activityHomeBinding.title = "Hau"

        handlers = MyClickHandlers(this);
        activityHomeBinding.handlers = handlers
    }

    inner class MyClickHandlers(internal var context: Context) {

        fun onTitleClicked(view: View) {
            Toast.makeText(applicationContext, "Title clicked!", Toast.LENGTH_SHORT).show()
        }

    }

}
