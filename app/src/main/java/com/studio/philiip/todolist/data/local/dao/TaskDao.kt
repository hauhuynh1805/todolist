package com.studio.philiip.todolist.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.studio.philiip.todolist.data.local.entity.TaskEntity


@Dao
interface TaskDao {
    @get:Query("SELECT * FROM task_table ORDER BY position DESC")
    val allTasks: LiveData<List<TaskEntity>>

    @Insert
    fun insertTask(TaskEntity: TaskEntity)

    @Update
    fun updateTask(TaskEntity: TaskEntity)

    @Delete
    fun deleteTask(TaskEntity: TaskEntity)

    @Query("DELETE FROM task_table")
    fun deleteAllTask()
}