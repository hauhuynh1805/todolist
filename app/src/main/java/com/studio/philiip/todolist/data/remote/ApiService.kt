package com.studio.philiip.todolist.data.remote

import com.studio.philiip.todolist.data.local.entity.TaskEntity
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService  {
    @GET("cards/{page}")
    fun loadTask(@Path("page") page: Int): Call<List<TaskEntity>>
}