package com.studio.philiip.todolist.data.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "task_table")
data class TaskEntity(
    @PrimaryKey val id: Int,
    val kind: String?,
    val etag: String?,
    val title: String?,
    val updated: Date?,
    val selfLink: String?,
    val parent: String?,
    val position: String?,
    val notes: String?,
    val status: String?,
    val due: Date?,
    val completed: Date?,
    val deleted: Boolean = false,
    val hidden: Boolean = false,
    val links: List<Links>
) {}
