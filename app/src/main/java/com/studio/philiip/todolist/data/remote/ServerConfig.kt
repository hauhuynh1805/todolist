package com.studio.philiip.todolist.data.remote

class ServerConfig {
    companion object {
        const val SERVER_HOST = "https://www.googleapis.com/"
        const val SERVER_TIMEOUT: Long = 30 * 1000
    }
}